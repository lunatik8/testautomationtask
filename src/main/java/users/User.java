package users;
import utils.RandomData;

public class User {
    RandomData string;
    String email;
    String password;
    String username;

    String firstName;
    String lastName;
    String phone;
    String position;
    String organizationName;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPosition() {
        return position;
    }

    public String getPhone() {
        return phone;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public User (String email, String password){
        this.email = email;
        this.password = password;
    }

    public User (){
        this.string = new RandomData();
        this.username = string.randomStringGenerator();
        this.password = "Test1234";
        this.firstName = string.randomStringGenerator();
        this.lastName = string.randomStringGenerator();
        this.phone = Integer.toString(string.randomIntGenerator());
        this.email = string.randomStringGenerator() + "@test.com";
        this.position = string.randomStringGenerator();
        this.organizationName = string.randomStringGenerator();
    }


}
