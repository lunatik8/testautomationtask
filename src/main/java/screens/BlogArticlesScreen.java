package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;
import utils.RandomData;

public class BlogArticlesScreen extends ProfileMenuItems {
    RandomData data = new RandomData();

    By createButton = By.xpath("//div[1]/section/div[3]/div/div[1]/a");

    By titleEngField = By.xpath("//*[@id=\"tab-en\"]/div/div/div[1]/input");
    By shortDescriptionField = By.xpath("//*[@id=\"tab-en\"]/div/div/div[2]/input");
    By frameClassName = By.xpath("//*[@id=\"cke_1_contents\"]/iframe");
    By contentField = By.xpath("/html/body");
    By metaTitleField = By.xpath("//*[@id=\"tab-en\"]/div/div/div[4]/input");
    By metaDescriptionField = By.xpath("//*[@id=\"tab-en\"]/div/div/div[5]/textarea");
    By metaKeywordsField = By.xpath("//*[@id=\"tab-en\"]/div/div/div[6]/textarea");
    By saveBlogArticleButton = By.xpath("//*[@id=\"job_form\"]/div/div/div[5]/div/button");
    By uploadCustomImage = By.id("cover_image");
    By blogElementCount = By.xpath("//div[3]/div/div[2]/table/tbody/tr");
    By deleteBlogButton = By.xpath("//div[2]/table/tbody/tr[1]/td[4]/div/form/button/img");

    private int blogCount = 0;
    String imageAbsPath = "C:\\Users\\Lusine\\IdeaProjects\\TestAutomationTask\\src\\main\\blog_article_image.jpg";


    public int getBlogCount() {
        this.blogCount = utils.findElementsCount(blogElementCount);
        return this.blogCount;
    }

    public BlogArticlesScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnCreateButton(){
        utils.click(createButton);
    }

    public void clickOnDeleteBlogButton(){
        utils.click(deleteBlogButton);
    }

    public void clickOnSaveBlogArticleButton(){
        utils.click(saveBlogArticleButton);
    }

    public void createEngBlogArticleIndividual(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnBlogArticlesIndividual();
        Thread.sleep(1000);
        createBlogArticle();
    }

    public void createEngBlogArticleOrganization(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnBlogArticlesOrganization();
        Thread.sleep(2000);
        createBlogArticle();

    }

    public void createBlogArticle() throws InterruptedException {
        clickOnCreateButton();
        Thread.sleep(3000);
        utils.sendKeys(titleEngField, data.randomStringGenerator());
        utils.sendKeys(shortDescriptionField, data.randomStringGenerator());
        utils.switchToFrame(frameClassName);
        utils.sendKeys(contentField, data.randomStringGenerator());
        utils.switchToDefaultContent();
        utils.sendKeys(metaTitleField, data.randomStringGenerator());
        utils.sendKeys(metaDescriptionField, data.randomStringGenerator());
        utils.sendKeys(metaKeywordsField, data.randomStringGenerator());
        utils.sendKeysImage(uploadCustomImage, imageAbsPath);
        clickOnSaveBlogArticleButton();
    }


    public void deleteBlogArticleOrganization(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnBlogArticlesOrganization();
        Thread.sleep(1000);
        if (getBlogCount() == 0){
            createBlogArticle();
            Thread.sleep(1000);
        }
        clickOnDeleteBlogButton();
        utils.alertAccept();

    }

}
