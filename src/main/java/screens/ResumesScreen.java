package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import users.User;
import utils.RandomData;

import java.util.List;

public class ResumesScreen extends LoginScreen {
    RandomData data = new RandomData();

    By dropdownButton = By.id("dropdownMenuLink");
    By resumes = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/div/div/a[4]");
    By coverLetterButton = By.xpath("/html/body/div[1]/section/div[3]/div/div[1]/div/a");
    By coverLetterText = By.xpath("*[@id=\"cke_1_contents\"]");
    By saveButton = By.id("apply_submit");
    By deleteButton = By.xpath("/html/body/div[1]/section/div[3]/div/div[2]/table/tbody/tr[2]/td[6]/div/form/button");


    By createButton = By.xpath("/html/body/div[1]/section/div[3]/div/div[1]/a");
    By nextButton = By.xpath("//*[@id=\"example-basic\"]/div[3]/ul/li[2]/a");
    By middleNameFiled = By.id("middle_name");
    By titleDropdownFile = By.id("title");
    By phoneType = By.id("phone_type_0");
    By addressFiled = By.xpath("//*[@id=\"address1\"]");
    By addressType = By.id("address_type_1");
    By country = By.id("address-country1");
    By cityFiled = By.id("City1");
    By postalCodeFiled = By.id("zip1");
    By addNewAddress =  By.id("add3");
    By deleteAddress =  By.id("2");

    By educationNameFiled = By.id("edu_name");
    By educationCityFiled = By.id("edu_city");
    By educationSpecify =By.id("edu_type");
    By educationCountry = By.id("edu_country");
    By educationDepartmentFiled = By.id("department");
    By educationStudyArea = By.id("study_area");
    By educationMajorFiled = By.id("major_field");
    By educationDegree = By.id("degree");
    By educationYearFrom = By.id(("edu_year_from"));
    By educationMonthFrom = By.id("edu_month_from");
    By educationYearTo = By.id("edu_year_to");
    By educationMonthTo= By.id("edu_month_to");
    By saveButtonOfEducationPage = By.id("edu_add");
    By nextButtonForEducationPage = By.xpath("//*[@id=\"example-basic\"]/div[3]/ul/li[2]");

    By languages = By.id("language_id");
    By readingSkill = By.id("reading");
    By writingSkill = By.id("writing");
    By communicationSkill = By.id("communication");
    By chooseAsForeign = By.id("radio_foreign");
    By getChooseAsNative = By.id("radio_native");
    By totalStudyTime = By.id("total_study");
    By typing = By.id("typing");
    By shorthand = By.id("shorthand");
    By saveButtonLanguage = By.id("language_add");
    By nextButtonOfLanguagesPage = By.xpath("//*[@id=\"example-basic\"]/div[3]/ul/li[2]"); ////*[@id="example-basic"]/div[3]/ul/li[2]


    By CheckBox = By.xpath("//*[@id=\"cv_availability\"]/div/div[1]/div[3]/div[4]/label/i");
    By jobType = By.xpath("//*[@id=\"cv_availability\"]/div/div[1]/div[6]/div[2]/label/i");
    By travel = By.id("travel");
    By resumeVisibility = By.id("show_resume");
    By shortTermSalary = By.id("short_term_salary");
    By shortTermSalaryCurrency = By.id("short_currency");
    By shortTermSalaryPeriod =By.id("short_period");
    By longTermSalary = By.id("long_term_salary");
    By longTermSalaryCurrency = By.id("long_currency");
    By longTermSalaryPeriod =By.id("long_period");
    By time = By.id("time");
    By interestArea =By.xpath("//*[@id=\"interest_area\"]/option");
    By positionType = By.xpath("//*[@id=\"position_type\"]/option");
    By organizationType = By.id("org_type");
    By undesiredOrganization = By.id("undesired_org");
    By finishButton = By.id("finish4");

    By frameName = By.xpath("//*[@id=\"cke_1_contents\"]/iframe");
    By contentField = By.xpath("/html/body");

    By closeIcon = By.xpath("//*[@id=\"preview_resume\"]/div/div/div[1]/button/span");



    public ResumesScreen(WebDriver driver) { super(driver);}

    public void clickOnDropdownButton(){utils.click(dropdownButton);}
    public void clickOnResumes(){utils.click(resumes);}
    public void clickOnCoverLetterButton(){utils.click(coverLetterButton);}
    public void clickOnSaveButton(){utils.click(saveButton);}
    public void clickOnCreateButton(){utils.click(createButton);}
    public void clickOnNextButton(){utils.click(nextButton);}
    public void selectDropdownTitle (){utils.selectElement(titleDropdownFile,1);}
    public void selectPhoneType(){utils.selectElement(phoneType,0);}
    public void selectAddressType(){utils.selectElement(addressType,1);}
    public void selectCountry(){utils.selectElement(country,5);}
    public void clickOnAddNewAddress(){utils.click(addNewAddress);}
    public void clickOnDeleteAddress(){utils.click(deleteAddress);}

    public void createResume(User user) throws InterruptedException{
        login(user);
        clickOnDropdownButton();
        clickOnResumes();
        Thread.sleep(4000);
        clickOnCreateButton();
        contactInformationPartOfResume();
        educationPartOfResume();
        Thread.sleep(4000);
        languageSkillsPartOfResume(chooseAsForeign);
        Thread.sleep(4000);
        resumeAvailability();
    }


    public void contactInformationPartOfResume (){
        utils.sendKeys(middleNameFiled,data.randomStringGenerator());
        selectDropdownTitle();
        selectPhoneType();
        utils.sendKeys(addressFiled,data.randomStringGenerator());
        selectAddressType();
        selectCountry();
        utils.sendKeys(cityFiled,data.randomStringGenerator());
        utils.sendKeys(postalCodeFiled,"data.randomIntGenerator()");
        clickOnAddNewAddress();
        clickOnDeleteAddress();
        clickOnNextButton();
    }

    public void educationPartOfResume() throws InterruptedException {

        utils.sendKeys(educationNameFiled,data.randomStringGenerator());
        utils.sendKeys(educationCityFiled,data.randomStringGenerator());
        utils.selectElement(educationSpecify,4);
        utils.selectElement(educationCountry,10);
        utils.sendKeys(educationDepartmentFiled,data.randomStringGenerator());
        utils.selectElement(educationStudyArea,5);
        utils.sendKeys(educationMajorFiled,data.randomStringGenerator());
        utils.selectElement(educationDegree,3);
        utils.selectElement(educationYearFrom,6);
        utils.selectElement(educationMonthFrom,10);
        utils.selectElement(educationYearTo,2);
        utils.selectElement(educationMonthTo,7);
        utils.click(saveButtonOfEducationPage);
        Thread.sleep(1000);
        utils.click(nextButtonForEducationPage);

    }
    public void languageSkillsPartOfResume(By by) throws InterruptedException {
        utils.selectElement(languages,3);
        utils.selectElement(readingSkill, 1);
        utils.selectElement(writingSkill, 1);
        utils.selectElement(communicationSkill, 1);
        utils.click(by);
        utils.selectElement(totalStudyTime,8);
        int randomNumber = data.randomIntGenerator();
        utils.sendKeys(typing, (String.valueOf(randomNumber)));
        utils.sendKeys(shorthand, (String.valueOf(randomNumber)));
        utils.click(saveButtonLanguage);
        Thread.sleep(1000);
        utils.click(nextButtonOfLanguagesPage);
    }
    public void resumeAvailability () throws InterruptedException {
        utils.click(CheckBox);
        utils.click(jobType);
        utils.selectElement(travel,1);
        utils.selectElement(resumeVisibility,1);
        utils.sendKeys(shortTermSalary,String.valueOf(data.randomIntGenerator()));
        utils.selectElement(shortTermSalaryCurrency,2);
        utils.selectElement(shortTermSalaryPeriod,3);
        utils.sendKeys(longTermSalary,String.valueOf(data.randomIntGenerator()));
        utils.selectElement(longTermSalaryCurrency,2);
        utils.selectElement(longTermSalaryPeriod,3);
        utils.sendKeys(time,String.valueOf(data.randomIntGenerator()));
        utils.getElementFromList(interestArea);
        utils.getElementFromList(positionType);
        utils.selectElement(organizationType,4);
        utils.sendKeys(undesiredOrganization,data.randomStringGenerator());
        utils.click(finishButton);
        Thread.sleep(1000);
        utils.click(closeIcon);
        Thread.sleep(1000);
        utils.click(finishButton);

    }
    public void createCoverLetter(User user) throws  InterruptedException{
        login(user);
        clickOnDropdownButton();
        clickOnResumes();
        Thread.sleep(1000);
        clickOnCoverLetterButton();
        Thread.sleep(1000);
        utils.switchToFrame(frameName);
        utils.sendKeys(contentField,data.randomStringGenerator());
        utils.switchToDefaultContent();
        clickOnSaveButton();
        Thread.sleep(1000);

    }
    public  int resumesCount(WebDriver driver){
        List<WebElement> rowCount = driver.findElements(By.xpath(" /html/body/div[1]/section/div[3]/div/div[2]/table/tbody/tr"));
        return rowCount.size();
    }
    public void deleteResume(User user)throws InterruptedException{
        login(user);
        clickOnDropdownButton();
        clickOnResumes();
        Thread.sleep(4000);
        utils.click(deleteButton);


    }
}
