package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProfileMenuItems extends LoginScreen {

    By dropdownButton = By.id("dropdownMenuLink");


    By myProfileIndividual = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/div/div/a[1]");
    By blogArticlesIndividual = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/div/div/a[2]");
    By resourcesIndividual = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/div/div/a[3]");
    By resumesIndividual = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/div/div/a[4]");
    By logoutIndividual = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/div/div/a[5]");

    By myProfileOrganization = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/div/div/a[1]");
    By blogArticlesOrganization = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/div/div/a[2]");
    By resourcesOrganization = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/div/div/a[3]");
    By applicantsOrganization = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/div/div/a[4]");
    By recruitmentOrganization = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/div/div/a[5]");
    By logoutOrganization = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/div/div/a[6]");
    By emailSubscriptionsIndividual = By.xpath("/html/body/div[1]/section/div[1]/ul/li[4]/a");
    By emailSubscriptionsOrganization= By.xpath("/html/body/div[1]/section/div[1]/ul/li[4]/a");

    public ProfileMenuItems(WebDriver driver) {
        super(driver);
    }

    public void clickOnDropButton(){
        utils.click(dropdownButton);
    }

    public void clickOnMyProfileIndividual(){
        utils.click(myProfileIndividual);
    }

    public void clickOnBlogArticlesIndividual(){
        utils.click(blogArticlesIndividual);
    }

    public void clickOnResourcesIndividual(){
        utils.click(resourcesIndividual);
    }

    public void clickOnResumesIndividual(){
        utils.click(resumesIndividual);
    }

    public void clickOnLogoutIndividual(){
        utils.click(logoutIndividual);
    }


    public void clickOnMyProfileOrganization(){
        utils.click(myProfileOrganization);
    }

    public void clickOnBlogArticlesOrganization(){
        utils.click(blogArticlesOrganization);
    }

    public void clickOnResourcesOrganization(){
        utils.click(resourcesOrganization);
    }

    public void clickOnApplicantsOrganization(){
        utils.click(applicantsOrganization);
    }

    public void clickOnRecruitmentOrganization(){
        utils.click(recruitmentOrganization);
    }

    public void clickOnLogoutOrganization(){
        utils.click(logoutOrganization);
    }

    public void clickOnEmailSubscriptionsIndividual(){utils.click(emailSubscriptionsIndividual);}
    public void clickOnEmailSubscriptionsOrganization(){utils.click(emailSubscriptionsOrganization);}
}
