package screens;

import org.openqa.selenium.WebDriver;
import users.User;

public class Logout extends ProfileMenuItems {

    public Logout(WebDriver driver) {
        super(driver);
    }


    public void logoutIndividual(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnLogoutIndividual();
    }

    public void logoutOrganization(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnLogoutOrganization();
    }


}
