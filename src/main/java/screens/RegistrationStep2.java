package screens;

import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;

import java.io.FileWriter;
import java.io.IOException;

public class RegistrationStep2 extends RegistrationScreen {
    By firstNameField = By.id("first_name");
    By lastNameField = By.id("last_name");
    By countryDrop = By.xpath("//div[1]/div[6]/select/option");
    By positionField = By.id("position");
    By organizationField = By.id("org_name");
    By phoneField = By.id("phone-0");
    By phoneTypeDrop = By.xpath("//*[@id=\"phone_type_0\"]/option");
    By emailField = By.id("email");
    By registerButton = By.id("register");


    public RegistrationStep2(WebDriver driver) {
        super(driver);
    }

    public void clickOnRegisterButton(){
        utils.click(registerButton);
    }


    public String registration(User user) throws IOException {
        String selectedUserCategory = registrationStep1(user);
        System.out.println("registration user category: "+ selectedUserCategory);
        if(selectedUserCategory.contentEquals("Individual")){
            utils.getElementFromList(countryDrop);
        }else if (selectedUserCategory.contentEquals("Organization") ){
            utils.sendKeys(positionField, user.getPosition());
            utils.sendKeys(organizationField, user.getOrganizationName());
        }
        utils.sendKeys(firstNameField, user.getFirstName());
        utils.sendKeys(lastNameField, user.getLastName());
        utils.sendKeys(phoneField, user.getPhone());
        utils.getElementFromList(phoneTypeDrop);
        utils.sendKeys(emailField, user.getEmail());
        clickOnRegisterButton();
        jsonObjBuilder(user, selectedUserCategory);
        return selectedUserCategory;

    }

    public void jsonObjBuilder(User user, String category) throws IOException {
        JSONObject jsObject = new JSONObject();
        jsObject.put("Category", category);
        jsObject.put("Username", user.getUsername());
        jsObject.put("Email", user.getEmail());
        jsObject.put("Password", user.getPassword());

        try{
            FileWriter file = new FileWriter("C://Users//Lusine//IdeaProjects//TestAutomationTask//src//main//Users.txt", true);
            file.write(jsObject.toJSONString());
            file.write("\r\n");
            file.close();
        }catch(IOException e){
            e.printStackTrace();
        }

    }




}
