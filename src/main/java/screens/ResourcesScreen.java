package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;

public class ResourcesScreen extends ProfileMenuItems {
    String urlResource = "https://www.helsinki.fi/en/university";

    By createResourceButton = By.xpath("//div[1]/section/div[3]/div/div[1]/a");
    By urlField = By.xpath("//*[@id=\"job_form\"]/div/div/div[1]/div/div[1]/input");
    By resourceCategory = By.xpath("//*[@id=\"category_id\"]/option");
    By subCategory = By.xpath("//*[@id=\"subcategory\"]/option");
    By nameField = By.xpath("//*[@id=\"tab-en\"]/div/div/div[1]/input");
    By submitResourceButton = By.xpath("//*[@id=\"job_form\"]/div/div/div[5]/div/div/button");
    By resourceElementCount = By.xpath("//div[2]/table/tbody/tr[1]/td[1]");

    private int resourceCount = 0;

    public int getResourceCount() {
        this.resourceCount = utils.findElementsCount(resourceElementCount);
        return this.resourceCount;
    }


    public ResourcesScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnCreateResourceButton(){
        utils.click(createResourceButton);
    }

    public void clickOnSubmitResourceButton(){
        utils.click(submitResourceButton);
    }

    public String createResourceIndividual(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnResourcesIndividual();
        Thread.sleep(1000);
        clickOnCreateResourceButton();
        utils.sendKeys(urlField, urlResource);
        String selectedResourceCategory = utils.getElementFromList(resourceCategory);
        if(selectedResourceCategory.contentEquals("Educational")){
            utils.getElementFromList(subCategory);
        }
        utils.sendKeys(nameField, "New Resource");
        clickOnSubmitResourceButton();

        return selectedResourceCategory;
    }

    public String createResourceOrganization(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnResourcesOrganization();
        Thread.sleep(1000);
        clickOnCreateResourceButton();
        utils.sendKeys(urlField, urlResource);
        String selectedResourceCategory = utils.getElementFromList(resourceCategory);
        if(selectedResourceCategory.contentEquals("Educational")){
            utils.getElementFromList(subCategory);
        }
        utils.sendKeys(nameField, "New Resource");
        clickOnSubmitResourceButton();

        return selectedResourceCategory;
    }
}
