package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import users.User;
import utils.RandomData;

public class BankDetailsScreen extends ProfileMenuItems {
    RandomData data = new RandomData();

    By bankDetails = By.id("bank-tab");
    By checkbox = By.id("individual");
    By individualCheckbox = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[1]/label");
    By customerField = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[2]/input");
    By addressField = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[5]/input");
    By passportField = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[11]/input");
    By legalAddressField = By.id("legal_address");
    By physicalAddressField = By.id("physical_address");
    By bankField = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[6]/input");
    By tinField = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[7]/input");
    By accountField = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[8]/input");
    By signingPerson1Field = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[9]/div[1]/div/input");
    By signingPerson2Field = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[10]/div[1]/div/input");
    By position1Field = By.xpath("//*[@id=\"position_1\"]/option");
    By position2Field = By.xpath("//*[@id=\"position_2\"]/option");
    By saveBankDetailsButton = By.xpath("//*[@id=\"bank\"]/div[1]/div/form/div[12]/div/button");


    public BankDetailsScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnBankDetails(){
        utils.click(bankDetails);
    }

    public boolean individualCheckboxSelected(){
        WebElement element = utils.findElement(checkbox);
        return element.isSelected();
    }

    public void clickOnIndividualCheckbox(){
        utils.click(individualCheckbox);
    }

    public void clickSaveBankDetailsButton(){
        utils.click(saveBankDetailsButton);
    }


    public void addBankDetailsIndividual(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnMyProfileOrganization();
        Thread.sleep(1000);
        clickOnBankDetails();
        if(!individualCheckboxSelected()) {
            clickOnIndividualCheckbox();
        }
        utils.sendKeys(customerField, data.randomStringGenerator());
        utils.sendKeys(addressField, data.randomStringGenerator());
        utils.sendKeys(passportField, data.randomStringGenerator());
        clickSaveBankDetailsButton();
    }


    public void addBankDetails(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnMyProfileOrganization();
        Thread.sleep(1000);
        clickOnBankDetails();
        if(individualCheckboxSelected()) {
            clickOnIndividualCheckbox();
        }
        utils.sendKeys(customerField, data.randomStringGenerator());
        utils.sendKeys(legalAddressField, data.randomStringGenerator());
        utils.sendKeys(physicalAddressField, data.randomStringGenerator());
        utils.sendKeys(bankField, data.randomStringGenerator());
        utils.sendKeys(tinField, data.randomStringGenerator());
        utils.sendKeys(accountField, data.randomStringGenerator());
        utils.sendKeys(signingPerson1Field, data.randomStringGenerator());
        utils.getElementFromList(position1Field);
        utils.sendKeys(signingPerson2Field, data.randomStringGenerator());
        utils.getElementFromList(position2Field);
        clickSaveBankDetailsButton();
    }


}
