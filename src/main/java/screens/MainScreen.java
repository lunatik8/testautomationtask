package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainScreen extends Screen {
    By regButton = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/a");
    By loginButton = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/a");
    By announcementTitleFiled = By.id("search");
    By searchJobButton = By.xpath("/html/body/div[1]/section[1]/div/form/button");

    public MainScreen(WebDriver driver){
        super(driver);
    }

    public void clickOnLoginButton(){
        utils.click(loginButton);
    }

    public void clickOnRegButton(){
        utils.click(regButton);
    }

    public void clickOnAnnouncementTitleFiled(){utils.click(announcementTitleFiled);}
    public void clickOnSearchButton(){utils.click(searchJobButton);}
    public void announcements(String string){
        clickOnAnnouncementTitleFiled();
        utils.sendKeys(announcementTitleFiled,string);
        clickOnSearchButton();
    }


}
