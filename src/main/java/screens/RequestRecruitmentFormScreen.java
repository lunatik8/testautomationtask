package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;

public class RequestRecruitmentFormScreen extends ProfileMenuItems {

    By createRecruitmentButton = By.xpath("//div[3]/div/div[1]/a");
    By selectQuantity = By.xpath("//*[@id=\"job\"]/div/div[1]/div[1]/div[1]/div/select/option");
    By selectCandidate = By.xpath("//*[@id=\"candidate-type\"]/option");
    By candidateTitleField = By.id("candidate-title");
    By term = By.xpath("//*[@id=\"term\"]/option");
    By workDaysField = By.id("work-days");
    By durationTerm = By.xpath("//*[@id=\"duration-term\"]/option");
    By durationType = By.xpath("//*[@id=\"duration-type\"]/option");
    By addressField = By.id("location-address");
    By cityField = By.id("location-city");
    By country = By.xpath("//*[@id=\"location-country-id\"]/option");
    By jobDescriptionField = By.id("job-description");
    By jobRequirementsField = By.id("job-requirements");
    By salaryField = By.id("salary");
    By currency = By.xpath("//*[@id=\"salary-currency\"]/option");
    By salaryBase = By.xpath("//*[@id=\"salary-base\"]/option");
    By salaryType = By.xpath("//*[@id=\"salary-type\"]/option");
    By previewButton = By.xpath("//*[@id=\"job\"]/div/div[2]/div[10]/div[1]/button");
    By submitButton = By.xpath("//div[3]/div/div/div[1]/form/button");


    public RequestRecruitmentFormScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnCreateRecruitmentButton(){
        utils.click(createRecruitmentButton);
    }

    public void clickOnPreviewButton(){
        utils.click(previewButton);
    }

    public void clickOnSubmitButton(){
        utils.click(submitButton);
    }

    public String requestRecruitment(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnRecruitmentOrganization();
        Thread.sleep(1000);
        clickOnCreateRecruitmentButton();
        utils.getElementFromList(selectQuantity);
        String candidate = utils.getElementFromList(selectCandidate);
        utils.sendKeys(candidateTitleField, candidate);
        utils.getElementFromList(term);
        utils.sendKeys(workDaysField, "40");
        utils.getElementFromList(durationTerm);
        utils.getElementFromList(durationType);
        utils.sendKeys(addressField, "Main street");
        utils.sendKeys(cityField, "City");
        utils.getElementFromList(country);
        utils.sendKeys(jobDescriptionField, "Software test engineer");
        utils.sendKeys(jobRequirementsField, "Java, Selenium");
        utils.sendKeys(salaryField, "100000");
        utils.getElementFromList(currency);
        utils.getElementFromList(salaryBase);
        utils.getElementFromList(salaryType);
        clickOnPreviewButton();
        Thread.sleep(1000);
        clickOnSubmitButton();

        return candidate;

    }
}
