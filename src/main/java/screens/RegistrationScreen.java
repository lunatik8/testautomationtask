package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;

public class RegistrationScreen extends MainScreen {
    By usernameField = By.id("name");
    By password1Field = By.id("password");
    By password2Field = By.id("password-confirm");
    By userCategoryDrop = By.xpath("//*[@id=\"is_organization\"]/option");

    By agreeCheckbox = By.xpath("//form/div[5]/label/i");
    By nextButton = By.xpath("//form/div[6]/button");

    public RegistrationScreen(WebDriver driver) {
        super(driver);
    }

    public void agreeCheckbox(){
        utils.click(agreeCheckbox);
    }

    public void nextButton(){
        utils.click(nextButton);
    }

    public String registrationStep1(User user){
        clickOnRegButton();
        utils.sendKeys(usernameField, user.getEmail());
        utils.sendKeys(password1Field, user.getPassword());
        utils.sendKeys(password2Field, user.getPassword());
        String userCategory = utils.getElementFromList(userCategoryDrop);
        agreeCheckbox();
        nextButton();
        return userCategory;

    }
}
