package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ForgotPasswordScreen extends MainScreen {
     By forgotPassword = By.xpath("//div/form/div[3]/p/a");
     By emailField = By.id("email");
     By resetPasswordButton = By.xpath("//div/form/div[2]/button");


    public ForgotPasswordScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnForgotPassword(){
        utils.click(forgotPassword);
    }
    public void clickOnSendResetPasswordButton(){
        utils.click(resetPasswordButton);
    }

    public void resetPassword(String email) throws InterruptedException {
        clickOnLoginButton();
        clickOnForgotPassword();
        Thread.sleep(2000);
        utils.sendKeys(emailField, email);
        clickOnSendResetPasswordButton();

    }

}
