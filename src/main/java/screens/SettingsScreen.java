package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;

public class SettingsScreen extends ProfileMenuItems {
    By settingsButton = By.id("company-tab");
    By currentPassword = By.xpath("//*[@id=\"company\"]/div[1]/div/form/div[1]/input");
    By newPassword = By.id("password");
    By confirmPassword = By.id("password-confirm");
    By saveChangesButton = By.xpath("//*[@id=\"company\"]/div[1]/div/form/div[4]/div/button");

    public SettingsScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnSettingsButton() {
        utils.click(settingsButton);
    }

    public void clickOnSaveChangesButton() {
        utils.click(saveChangesButton);
    }

    public void updateSettingsForIndividual(User user, String password) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnMyProfileIndividual();
        Thread.sleep(1000);
        clickOnSettingsButton();
        utils.sendKeys(currentPassword, user.getPassword());
        utils.sendKeys(newPassword, password);
        utils.sendKeys(confirmPassword, password);
        user.setPassword(password);
        clickOnSaveChangesButton();
    }

    public void updateSettingsForOrganization(User user, String password) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnMyProfileOrganization();
        Thread.sleep(1000);
        clickOnSettingsButton();
        utils.sendKeys(currentPassword, user.getPassword());
        utils.sendKeys(newPassword, password);
        utils.sendKeys(confirmPassword, password);
        user.setPassword(password);
        clickOnSaveChangesButton();
    }
}

