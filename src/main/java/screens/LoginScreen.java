package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;

public class LoginScreen extends MainScreen {
    By loginField = By.xpath("//*[@id=\"login\"]");
    By passField = By.xpath("//*[@id=\"password\"]");
    By login = By.xpath("//div/div/form/div[4]/button");


    public LoginScreen(WebDriver driver) {

        super(driver);
    }

    public void clickOnLogin(){
        utils.click(login);
    }

    public void login (User user) throws InterruptedException {
        clickOnLoginButton();
        Thread.sleep(1000);
        utils.sendKeys(loginField, user.getEmail());
        utils.sendKeys(passField, user.getPassword());
        clickOnLogin();

    }
}
