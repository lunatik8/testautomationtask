package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;
import utils.RandomData;

public class ContactInformationScreen extends ProfileMenuItems {
    RandomData data = new RandomData();

    By title = By.xpath("//*[@id=\"title\"]/option");
    By middleName = By.id("middle_name");
    By cityzenship = By.xpath("//*[@id=\"job\"]/form/div/div[2]/div[3]/select/option");

    By orgShortName = By.id("short_name");
    By legalStructure = By.xpath("//*[@id=\"legal_structure\"]/option");
    By fieldsOfActivity = By.xpath("//*[@id=\"fields_of_activity\"]/option");
    By numberOfEmployees = By.xpath("//*[@id=\"number_of_employees\"]/option");

    By saveButtonIndividual = By.xpath("//div/div[4]/div/div/button");
    By saveButtonOrganization =By.xpath("//div[3]/div/div/button");


    public ContactInformationScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnSaveIndividual(){
        utils.click(saveButtonIndividual);
    }

    public void clickOnSaveOrganization(){
        utils.click(saveButtonOrganization);
    }

    public void updateContactInfoIndividual(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnMyProfileIndividual();
        Thread.sleep(3000);
        utils.getElementFromList(title);
        utils.sendKeys(middleName, data.randomStringGenerator());
        utils.getElementFromList(cityzenship);
        clickOnSaveIndividual();
    }

    public void updateContactInfoOrganization(User user) throws InterruptedException {
        login(user);
        clickOnDropButton();
        clickOnMyProfileOrganization();
        Thread.sleep(3000);
        utils.getElementFromList(title);
        utils.sendKeys(middleName, data.randomStringGenerator());
        utils.sendKeys(orgShortName, data.randomStringGenerator());
        utils.getElementFromList(legalStructure);
        utils.getElementFromList(fieldsOfActivity);
        utils.getElementFromList(numberOfEmployees);
        clickOnSaveOrganization();
    }

}
