package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import users.User;

public class PostAnnouncementScreen extends LoginScreen{
    By postAnnouncementButton = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/a/span");
    By selectAnnouncement = By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/div/a");

    public PostAnnouncementScreen(WebDriver driver) {
        super(driver);
    }

    public void clickOnPostAnnouncementButton(){
        utils.click(postAnnouncementButton);
    }

    public void postAnnouncement(User user) throws InterruptedException {
        login(user);
        clickOnPostAnnouncementButton();
        utils.clickOnElementFromList(selectAnnouncement);

    }


}
