package screens;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import users.User;

public class EmailSubscriptionsScreen extends ProfileMenuItems {
    By deliveryDropdownButton = By.id("delivery");
    By category = By.id("category");
    By announcementsType = By.id("type");
    By languages = By.id("languages");
    By submitButton = By.xpath("//*[@id=\"job_form\"]/div/div/div[5]/div/div/button");
    By oneCategory = By.xpath("//*/ul/li[2]/span[2]");
    By clickOnScreen = By.xpath("//*[@id=\"status-form-1\"]/h4");

    public void selectDeliveryDropdownButton(){utils.selectElement(deliveryDropdownButton,2);}

    public void selectAnnouncementsType(){utils.selectElement(announcementsType,5);}
    public void selectLanguages(){utils.selectElement(languages,3);}
    public void clickOnSubmitButton(){utils.click(submitButton);}

    public void clickOnCategory(){utils.click(category);}

    public void clickOnOneCategory(){utils.click(oneCategory);}

    public void clickOnScreen(){utils.click(clickOnScreen);}

    public boolean categoryCheckboxSelected(){
        WebElement element = utils.findElement(oneCategory);
        return element.isSelected();
    }


    public EmailSubscriptionsScreen(WebDriver driver){super(driver);}
    public void emailSubscriptionIndividual(User user) throws InterruptedException{
        login(user);
        clickOnDropButton();
        clickOnMyProfileIndividual();
        Thread.sleep(1000);
        clickOnEmailSubscriptionsIndividual();
        selectDeliveryDropdownButton();
        clickOnCategory();
        Thread.sleep(1000);
        if(!categoryCheckboxSelected()) {
            clickOnOneCategory();
        }
        Thread.sleep(1000);
        clickOnScreen();
        Thread.sleep(1000);
        selectAnnouncementsType();
        selectLanguages();
        clickOnSubmitButton();

    }
    public void emailSubscriptionOrganization(User user) throws InterruptedException{
        login(user);
        clickOnDropButton();
        clickOnMyProfileOrganization();
        Thread.sleep(1000);
        clickOnEmailSubscriptionsOrganization();
        selectDeliveryDropdownButton();
        clickOnCategory();
        if(!categoryCheckboxSelected()) {
            clickOnOneCategory();
        }
        clickOnScreen();
        Thread.sleep(1000);
        selectAnnouncementsType();
        selectLanguages();
        clickOnSubmitButton();

    }
}
