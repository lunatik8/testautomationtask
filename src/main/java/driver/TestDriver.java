package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class TestDriver {
    protected WebDriver webDriver;
    String url = "https://www.careercenter.am/en";

    public void initialDriver(){
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        webDriver.get(url);

    }

//    public Cookie loggedInDriver(User user) throws InterruptedException {
//        WebDriverManager.chromedriver().setup();
//        webDriver = new ChromeDriver();
//        webDriver.manage().window().maximize();
//        webDriver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
//        webDriver.get(url);
//
//        Set<Cookie> cookies = webDriver.manage().getCookies();
//        Iterator<Cookie> cookieList =  cookies.iterator();
//        Cookie cookie = null;
//        while (cookieList.hasNext()) {
//            cookie = cookieList.next();
//            if(cookie.getName().contentEquals("careercenter_session")){
//                System.out.println(cookie);
////                webDriver.manage().addCookie(cookie);
////                webDriver.get(url);
//            }
//
//        }
//        return cookie;
//
//    }

    @BeforeMethod
    public void setUpDriver(){
        initialDriver();
    }

    @AfterMethod
    public void testQuit(){
//        webDriver.quit();

    }

}
