package utils;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Random;

public class UtilsSelenium {
    WebDriver driver;
    WebElement element;
    Select option;

    public UtilsSelenium(WebDriver driver){
        this.driver = driver;
    }

    public void switchToFrame(By by){
        driver.switchTo().frame(driver.findElement(by));
    }

    public void switchToDefaultContent(){
        driver.switchTo().defaultContent();
    }

    public void click(By by) {
        element = driver.findElement(by);
        element.click();
    }

    public void sendKeys(By by, String string ){
        element = driver.findElement(by);
        element.clear();
        element.sendKeys(string);
    }

    public void selectElement(By by, int index){
        element = driver.findElement(by);
        option = new Select( driver.findElement(by));
        option.selectByIndex(index);
    }

    public WebElement findElement(By by){
        element = driver.findElement(by);
        return element;
    }

    public void sendKeysImage(By by, String string){
        element = driver.findElement(by);
        element.sendKeys(string);
    }


    public int findElementsCount(By by){
        return driver.findElements(by).size();
    }

    public void alertAccept(){
        Alert alt = driver.switchTo().alert();
        alt.accept();
    }


    public String getElementFromList(By by){
        Random random = new Random();
        List<WebElement> list = driver.findElements(by);
        int index = 0;
        if(list.size() > 1) {
            index = random.nextInt(list.size()-1) + 1;
        }
        list.get(index).click();
        return list.get(index).getText();
    }

    public void clickOnElementFromList(By by){
        Random random = new Random();
        List<WebElement> list = driver.findElements(by);
        int index = 0;
        if(list.size() > 1) {
            index = random.nextInt(list.size()-1) + 1;
        }
        list.get(index).click();
    }


}
