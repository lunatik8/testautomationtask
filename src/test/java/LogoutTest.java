import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.Logout;
import users.User;

public class LogoutTest extends TestDriver {
    User user;
    Logout logout;

    @BeforeMethod
    public void setup(){
        logout = new Logout(webDriver);
    }

    // TC-23: Checking Logout functionality for Individual user
    @Test
    public void testLogoutIndividual() throws InterruptedException {
        user = new User("lusine969@gmail.com", "Test1234");

        String expectedString = "Equal Opportunities for Everyone";
        logout.logoutIndividual(user);


        Assert.assertTrue(webDriver.getPageSource().contains(expectedString));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/a")).isDisplayed());


    }

    // TC-23: Checking Logout functionality for Organization user
    @Test
    public void testLogoutOrganization() throws InterruptedException {
        user = new User("lusine969", "Test1234");

        String expectedString = "Equal Opportunities for Everyone";
        logout.logoutOrganization(user);
        

        Assert.assertTrue(webDriver.getPageSource().contains(expectedString));
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[5]/a")).isDisplayed());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@id=\"navbarSupportedContent\"]/ul/li[4]/a")).isDisplayed());

    }
}
