import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.ResourcesScreen;
import users.User;

public class CreateResourceTest extends TestDriver {
    User user;
    ResourcesScreen resourcesScreen;


    @BeforeMethod
    public void setup(){
        resourcesScreen = new ResourcesScreen(webDriver);
    }

    // TC-17: Creating Resource of the Individual user
    @Test
    public void testCreateResourceIndividual() throws InterruptedException {
        user = new User("oJnuVg@test.com", "Test1234");
        String category = resourcesScreen.createResourceIndividual(user);

        String actualCategory = webDriver.findElement(By.xpath("//div[2]/table/tbody/tr[1]/td[2]")).getText();
        int actualCount = webDriver.findElements(By.xpath("//div[2]/table/tbody/tr[1]/td[1]")).size();
        int count = resourcesScreen.getResourceCount();

        Assert.assertEquals(actualCount, count);
        Assert.assertEquals(actualCategory, category);

    }

    // TC-18: Creating Resource of the Organization user
    @Test
    public void testCreateResourceOrganization() throws InterruptedException {
        user = new User("WOKIJR@test.com", "Test1234");
        String category = resourcesScreen.createResourceOrganization(user);

        String actualCategory = webDriver.findElement(By.xpath("//div[2]/table/tbody/tr[1]/td[2]")).getText();
        int actualCount = webDriver.findElements(By.xpath("//div[2]/table/tbody/tr[1]/td[1]")).size();
        int count = resourcesScreen.getResourceCount();

        Assert.assertEquals(actualCount, count);
        Assert.assertEquals(actualCategory, category);

    }
}
