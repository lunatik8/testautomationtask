import driver.TestDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.ResumesScreen;
import users.User;

public class DeleteResumeTest extends TestDriver {
    User user;
    ResumesScreen resumesScreen;
    @BeforeMethod
    public void setup(){resumesScreen= new ResumesScreen(webDriver);
    }
    @Test
    public void deleteResumeTest()throws InterruptedException{
        user = new User("oJnuVg@test.com", "Test1234");
        resumesScreen.deleteResume(user);
        webDriver.switchTo().alert().accept();
        int expectedResumesCount = 2;
        int actualResumesCount = resumesScreen.resumesCount(webDriver);
        Assert.assertEquals(expectedResumesCount,actualResumesCount);

    }
}
