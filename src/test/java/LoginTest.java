import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.LoginScreen;
import users.User;


public class LoginTest extends TestDriver {

    LoginScreen loginScreen;
    User user;

    @BeforeMethod
    public void setup(){
        user = new User("lusine969@gmail.com", "LunaTik86");
        loginScreen = new LoginScreen(webDriver);

    }

    // TC-01: Login with valid data
    @Test
    public void testLogin() throws InterruptedException {
        loginScreen.login(user);
        String actualEmail = webDriver.findElement(By.xpath("//*[@id=\"dropdownMenuLink\"]/span")).getText();

        Thread.sleep(2000);
        Assert.assertTrue(webDriver.getPageSource().contains(actualEmail));
        Assert.assertEquals(actualEmail.toLowerCase(), user.getEmail());
    }

    // TC-01: Login with invalid data
    @Test
    public void testLoginIndividual() throws InterruptedException {
        user = new User("invalidEmail", "invalidPassword");
        loginScreen.login(user);
        String actualError = webDriver.findElement(By.xpath("//div/div/form/div[1]/span")).getText();
        String expectedError = "These credentials do not match our records.";

        Assert.assertTrue(webDriver.getPageSource().contains(actualError));
        Assert.assertEquals(actualError, expectedError);

    }


}
