import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.ResumesScreen;
import users.User;

public class CreateResumeTest extends TestDriver {
    User user;
    ResumesScreen resumesScreen;


    @BeforeMethod
    public void setup(){
        resumesScreen = new ResumesScreen(webDriver);
    }


    @Test
    public void createResumeTest() throws InterruptedException{
        user = new User("oJnuVg@test.com", "Test1234");
        resumesScreen.createResume(user);

        Boolean emailButton = webDriver.findElement(By.xpath("//div[1]/section/a[3]")).isDisplayed();

        Assert.assertTrue(emailButton);


    }
}
