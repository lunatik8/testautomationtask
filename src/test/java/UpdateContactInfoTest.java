import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.ContactInformationScreen;
import users.User;

public class UpdateContactInfoTest extends TestDriver {

    User user;
    ContactInformationScreen contactScreen;

    @BeforeMethod
    public void setup(){
        contactScreen = new ContactInformationScreen(webDriver);
    }

    // TC-04: Updating the profile information of the Individual user
    @Test
    public void testUpdateInformationIndividual() throws InterruptedException {
        user = new User("oJnuVg@test.com", "Test1234");
        contactScreen.updateContactInfoIndividual(user);

        String actualMessage = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]")).getText();
        String expectedMessage = "Profile Updated Succcesfully";

        Assert.assertTrue(webDriver.getPageSource().contains(expectedMessage));
        Assert.assertEquals(actualMessage, expectedMessage);
    }

    // TC-05: Updating the profile information of the Organization user
    @Test
    public void testUpdateInformationOrganization() throws InterruptedException {
        user = new User("WOKIJR@test.com", "Test1234");
        contactScreen.updateContactInfoOrganization(user);

        String actualMessage = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]")).getText();
        String expectedMessage = "Profile Updated Succcesfully";

        Assert.assertTrue(webDriver.getPageSource().contains(expectedMessage));
        Assert.assertEquals(actualMessage, expectedMessage);
    }
}
