import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.SettingsScreen;
import users.User;

public class UpdateSettingsTest extends TestDriver {
    User user;
    SettingsScreen settingsScreen;

    @BeforeMethod
    public void setup(){
        settingsScreen = new SettingsScreen(webDriver);
    }

    // TC-06: Updating Settings (Password) for Individual user
    @Test
    public void testUpdateInformationIndividual() throws InterruptedException {
        user = new User("individual1", "Test1234");
        String newPassword = "individual1";
        //String newPassword = "Test1234";

        settingsScreen.updateSettingsForIndividual(user, newPassword);

        String actualMessage = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]")).getText();
        String expectedMessage = "Password updated successfully";


        Assert.assertTrue(webDriver.getPageSource().contains(expectedMessage));
        Assert.assertEquals(actualMessage, expectedMessage);
        Assert.assertEquals(newPassword, user.getPassword());

    }

    // TC-21: Updating Settings (Password) for Organization user
    @Test
    public void testUpdateInformationOrganization() throws InterruptedException {
        user = new User("WOKIJR@test.com", "Test12345");
        //String newPassword = "Test12345";
        String newPassword = "Test1234";

        settingsScreen.updateSettingsForOrganization(user, newPassword);

        String actualMessage = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]")).getText();
        String expectedMessage = "Password updated successfully";


        Assert.assertTrue(webDriver.getPageSource().contains(expectedMessage));
        Assert.assertEquals(actualMessage, expectedMessage);
        Assert.assertEquals(newPassword, user.getPassword());
    }
}
