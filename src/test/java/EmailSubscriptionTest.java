import driver.TestDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.EmailSubscriptionsScreen;
import users.User;

public class EmailSubscriptionTest extends TestDriver {
    User user;
    EmailSubscriptionsScreen emailSubscriptionsScreen;

    @BeforeMethod
    public void setup(){
        emailSubscriptionsScreen= new EmailSubscriptionsScreen(webDriver);
    }
    //TC_16
    @Test
    public void setEmailSubscriptionsIndividualTest ()throws InterruptedException{
        user = new User("oJnuVg@test.com", "Test1234");
        emailSubscriptionsScreen.emailSubscriptionIndividual(user);
        String expectedDeliveryMessage = "Individual Emails";
        String expectedAnnouncementTypeMessage = "Training";
        String expectedLanguageMessage = "English";
        Assert.assertTrue(webDriver.getPageSource().contains(expectedDeliveryMessage));
        Assert.assertTrue(webDriver.getPageSource().contains(expectedAnnouncementTypeMessage));
        Assert.assertTrue(webDriver.getPageSource().contains(expectedLanguageMessage));

    }
    //TC-15
    @Test
    public void setEmailSubscriptionsOrganizationTest ()throws InterruptedException{
        user = new User("WOKIJR@test.com", "Test1234");
        emailSubscriptionsScreen.emailSubscriptionOrganization(user);
        String expectedDeliveryMessage = "Individual Emails";
        String expectedAnnouncementTypeMessage = "Training";
        String expectedLanguageMessage = "English";
        Assert.assertTrue(webDriver.getPageSource().contains(expectedDeliveryMessage));
        Assert.assertTrue(webDriver.getPageSource().contains(expectedAnnouncementTypeMessage));
        Assert.assertTrue(webDriver.getPageSource().contains(expectedLanguageMessage));



    }
}
