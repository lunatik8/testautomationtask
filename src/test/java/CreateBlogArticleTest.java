import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.BlogArticlesScreen;
import users.User;

public class CreateBlogArticleTest extends TestDriver {
    User user;
    BlogArticlesScreen blogArticlesScreen;

    @BeforeMethod
    public void setup(){
        blogArticlesScreen = new BlogArticlesScreen(webDriver);
    }


    // TC-10: Creating Blog Article of the Individual user
    @Test
    public void testCreateEngBlogArticleIndividual() throws InterruptedException {
        user = new User("oJnuVg@test.com", "Test1234");
        blogArticlesScreen.createEngBlogArticleIndividual(user);
        int actualCount = webDriver.findElements(By.xpath("//div[3]/div/div[2]/table/tbody/tr")).size();
        int count1 = blogArticlesScreen.getBlogCount();

        Assert.assertEquals(actualCount, count1);
    }

    // TC-22: Creating Blog Article of the Organization user
    @Test
    public void testCreateEngBlogArticleOrganization() throws InterruptedException {
        user = new User("WOKIJR@test.com", "Test12345");
        blogArticlesScreen.createEngBlogArticleOrganization(user);
        int actualCount = webDriver.findElements(By.xpath("//div[3]/div/div[2]/table/tbody/tr")).size();
        int count1 = blogArticlesScreen.getBlogCount();

        Assert.assertEquals(actualCount, count1);

    }

}
