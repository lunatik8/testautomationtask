import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.RequestRecruitmentFormScreen;
import users.User;

public class RequestRecruitmentTest extends TestDriver {
    User user;
    RequestRecruitmentFormScreen requestRecruitmentFormScreen;

    @BeforeMethod
    public void setup(){
        requestRecruitmentFormScreen = new RequestRecruitmentFormScreen(webDriver);
    }

    // TC-19: Requesting Recruitment of the Organization user
    @Test
    public void testRequestRecruitmentOrganization() throws InterruptedException {
        user = new User("WOKIJR@test.com", "Test1234");
        String candidateTitle =  requestRecruitmentFormScreen.requestRecruitment(user);

        String actualJobTitle = webDriver.findElement(By.xpath("//div[2]/table/tbody/tr[1]/td[1]/a")).getText();

        Assert.assertEquals(actualJobTitle, candidateTitle);
    }

}
