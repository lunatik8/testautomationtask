import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.ForgotPasswordScreen;

public class ResetPasswordTest extends TestDriver {
    ForgotPasswordScreen resetPass;


    @BeforeMethod
    public void setup(){
        resetPass = new ForgotPasswordScreen(webDriver);

    }

    // TC-11: Verifying reset password with registered email
    @Test
    public void reset_password() throws InterruptedException {
        resetPass.resetPassword("doRbAU@test.com");
        Thread.sleep(1000);
        String actualText = webDriver.findElement(By.xpath("//*[@id=\"success_tic\"]/div/div/div/div/h5")).getText();
        String expectedText = "Your password reset instructions have been emailed to you";
        Assert.assertTrue(webDriver.getPageSource().contains(actualText));
        Assert.assertEquals(actualText, expectedText);

    }

    // TC-11: Verifying reset password with non registered email
    @Test
    public void reset_password_invalid_email() throws InterruptedException {
        resetPass.resetPassword("dshcsdjcdsnc@test.com");
        String actualText = webDriver.findElement(By.xpath("//div/form/div[1]/span")).getText();
        String expectedText = "We can't find a user with that e-mail address.";
        Assert.assertTrue(webDriver.getPageSource().contains(actualText));
        Assert.assertEquals(actualText, expectedText);
    }
}
