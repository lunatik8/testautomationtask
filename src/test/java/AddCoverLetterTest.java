import driver.TestDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.ResumesScreen;
import users.User;

public class AddCoverLetterTest extends TestDriver {
    User user;
    ResumesScreen resumesScreen;
    @BeforeMethod
    public void setup(){
        resumesScreen = new ResumesScreen(webDriver);
    }
    @Test
    public void testUpdateInformationIndividual() throws InterruptedException {
        user = new User("oJnuVg@test.com", "Test1234");
        resumesScreen.createCoverLetter(user);
        String expectedMessage = "Cover Letter Updated Successfully!";
        Assert.assertTrue(webDriver.getPageSource().contains(expectedMessage));

    }
}
