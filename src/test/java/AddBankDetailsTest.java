import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.BankDetailsScreen;
import users.User;

public class AddBankDetailsTest extends TestDriver {

    User user;
    BankDetailsScreen bankDetailsScreen;

    @BeforeMethod
    public void setup(){
        user = new User("WOKIJR@test.com", "Test1234");
        bankDetailsScreen = new BankDetailsScreen(webDriver);
    }

    // TC-13: Adding Bank Details of the Organization user
    @Test
    public void testAddBankDetails() throws InterruptedException {
        bankDetailsScreen.addBankDetails(user);

        String actualMessage = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]")).getText();
        String expectedMessage = "Bank Details Updated Successfully";

        Assert.assertTrue(webDriver.getPageSource().contains(expectedMessage));
        Assert.assertEquals(actualMessage, expectedMessage);
    }

    // TC-14: Adding Bank Details (Individual) of the Organization user
    @Test
    public void testAddBankDetailsIndividual() throws InterruptedException {
        bankDetailsScreen.addBankDetailsIndividual(user);

        String actualMessage = webDriver.findElement(By.xpath("/html/body/div[1]/div[2]")).getText();
        String expectedMessage = "Bank Details Updated Successfully";

        Assert.assertTrue(webDriver.getPageSource().contains(expectedMessage));
        Assert.assertEquals(actualMessage, expectedMessage);
    }
}
