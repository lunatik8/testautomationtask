import driver.TestDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.MainScreen;

public class SearchingJobTest extends TestDriver {
    MainScreen mainScreen;
    @BeforeMethod
    public void setup(){
        mainScreen = new  MainScreen(webDriver);
    }
    @Test
    public void SearchJobFalseTitle(){
        mainScreen.announcements("jks");
        Assert.assertTrue(webDriver.getPageSource().contains( "No Result is found"));
    }
    @Test
    public void SearchJobTrueTitle(){
        mainScreen.announcements("Office Manager");
        Assert.assertTrue(webDriver.getPageSource().contains("Office Manager"));
    }
}

