import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.RegistrationStep2;
import users.User;

import java.io.IOException;

public class RegistrationTest extends TestDriver {

    RegistrationStep2 regScreen;
    User user;

    @BeforeMethod
    public void setup(){
        user = new User();
        regScreen = new RegistrationStep2(webDriver);
    }

    // TC-02, TC-03: Registration as an Individual/Organization
    @Test
    public void testRegistration() throws IOException {
        String category = regScreen.registration(user);
        System.out.println("Category: " + category);
        String actualFirstName = webDriver.findElement(By.id("first_name")).getAttribute("value");
        String actualLastName = webDriver.findElement(By.id("last_name")).getAttribute("value");
        String actualPhoneNumber = webDriver.findElement(By.xpath("//*[@id=\"row4_1\"]/div[1]/input")).getAttribute("value");
        String actualEmail = webDriver.findElement(By.id("email")).getAttribute("value");


        Assert.assertEquals( actualFirstName, user.getFirstName());
        Assert.assertEquals( actualLastName, user.getLastName());
        Assert.assertEquals( actualPhoneNumber, user.getPhone());
        Assert.assertEquals(actualEmail, user.getEmail());

        if(category.contentEquals("Individual")){
            String resumesTab = webDriver.findElement(By.xpath("//div[1]/ul/li[5]/a")).getText();
            System.out.println("Resumes tab: " + resumesTab);

            Assert.assertTrue(webDriver.getPageSource().contains(resumesTab));

        }else if(category.contentEquals("Organization")){
            String actualPosition = webDriver.findElement(By.id("position")).getAttribute("value");
            String actualOrganizationName = webDriver.findElement(By.id("org_name")).getAttribute("value");
            String applicationsTab = webDriver.findElement(By.xpath("//section/div[1]/ul/li[5]/a")).getText();
            String recruitmentTab = webDriver.findElement(By.xpath("//section/div[1]/ul/li[6]/a")).getText();

            System.out.println("Applications tab: " + applicationsTab);
            System.out.println("Recruitment tab: " + recruitmentTab);

            Assert.assertEquals(actualPosition, user.getPosition());
            Assert.assertEquals(actualOrganizationName, user.getOrganizationName());
            Assert.assertTrue(webDriver.getPageSource().contains(applicationsTab));
            Assert.assertTrue(webDriver.getPageSource().contains(recruitmentTab));
        }




    }


}
