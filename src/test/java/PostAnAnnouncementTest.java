import driver.TestDriver;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import screens.PostAnnouncementScreen;
import users.User;

public class PostAnAnnouncementTest extends TestDriver {
    User user;
    PostAnnouncementScreen postAnnouncementScreen;

    @BeforeMethod
    public void setup(){
        postAnnouncementScreen = new PostAnnouncementScreen(webDriver);
    }

    // TC-20: Posting an announcement when have no access
    @Test
    public void testPostAnnouncement() throws InterruptedException {
        user = new User("WOKIJR@test.com", "Test1234");
        postAnnouncementScreen.postAnnouncement(user);

        String actualMessage = webDriver.findElement(By.xpath("//*[@id=\"fail-announcement\"]/div/div/div[2]/h5")).getText();
        String expectedMessage = "You don't have access for making announcement.";

        Assert.assertTrue(webDriver.getPageSource().contains(actualMessage));
        Assert.assertEquals(actualMessage, expectedMessage);
    }
}
